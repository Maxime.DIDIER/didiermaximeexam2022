import { StatusBar } from 'expo-status-bar';
import {FlatList, StyleSheet, Text, View, Image, TextInput, TouchableOpacity} from 'react-native';
import {useEffect, useState} from "react";
import {SafeAreaView} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";

const Stack = createBottomTabNavigator();

export default function App() {
    const [liste, setListe] = useState([]);
    const [count, setCount] = useState('1');
    const [name, setName] = useState('ho');
    const [aleatoire, setaleatoire] = useState([]);


    function ScreenFilter({route, navigation}) {/*
        if(route.params?.post){
            const citation = () => {
                fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=15&character=${route.params?.post}`)
                    .then(function (response) {
                        return response.json();
                    }).then(function (response) {
                    setListe(response)
                })
            }
        } else
            {*/
                const citation = () => {
                    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${count}&character=${name}`)
                        .then(function (response) {
                            return response.json();
                        }).then(function (response) {
                        setListe(response)
                    })
                //}
            }
        useEffect(()=>{
            liste && citation();
        }, [count])

        useEffect(()=>{
            liste && citation();
        }, [name])
        return (
            <View style={styles.container}>
                <Image source={{
                    uri: liste.image
                }} style={{width: 200}}>
                </Image>
                <SafeAreaView style={styles.safearea}>
                    <FlatList data={liste} renderItem={({item}) => (
                        <View style={styles.card}>
                            <Text style={styles.character}>Character : {item.character}</Text>
                            <Image style={styles.img} source={{
                                uri: item.image
                            }}/>
                            <Text style={styles.quote}>Quote : {item.quote}</Text>
                        </View>
                    )}/>
                </SafeAreaView>
                <View style={styles.viewinput}>
                    <TextInput
                        style={styles.input}
                        value={count}
                        onChangeText={setCount}
                        keyboardType="numeric"
                    />
                        <TextInput
                        style={styles.inputname}
                        value={name}
                        onChangeText={setName}
                    />
                </View>
                <View style={styles.viewbutton}>
                    <TouchableOpacity style={styles.buttonautoload}>
                        <Text style={styles.textbutton} onPress={citation}>Autoload</Text>
                    </TouchableOpacity>
                </View>
                <StatusBar style="auto"/>
            </View>
        );
    }

    function HomeScreen({navigation}){
        const aleatoirequote = () => {
            return fetch('https://thesimpsonsquoteapi.glitch.me/quotes')
                .then(function (response){
                    return response.json();
                }).then(function (response){
                    return response
                })
        }

        const add = () => {
            let promises = [];
            for(let i = 0; i<5; i++){
                promises.push(aleatoirequote());
            }
            Promise.all(promises).then((data) => {setaleatoire([...aleatoire, ...data])})
        }

        useEffect(()=>{
            add()
        }, [])

        return (
          <View>
              <FlatList
                  data={aleatoire}
                  onEndReached={add}
                  keyExtractor={item => Math.random().toString()}
                  renderItem={({item}) => (
                  <View style={styles.card}>
                      <TouchableOpacity style={styles.touchable} onPress={()=>navigation.navigate('Filter', {
                          params: {
                              post: item[0].name
                          }
                      })}>
                          <Text style={styles.character}>Character : {item[0].character}</Text>
                          <Image style={styles.img} source={{
                              uri: item[0].image
                          }}/>
                          <Text style={styles.quote}>Quote : {item[0].quote}</Text>
                      </TouchableOpacity>
                  </View>
              )}/>
          </View>
        );
    }
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={HomeScreen}/>
                <Stack.Screen name="Filter" component={ScreenFilter}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
  },
    touchable: {
      width: "90%",
        height: "90%"
    },
    safearea: {
      flex: 1
    },
    character: {
      color: "white",
    },
    quote: {
      color: "white",
    },
    titleview: {
        flex: 1,
        backgroundColor: "black"
    },
    img:{
        aspectRatio: 1,
        width: "50%",
        height: "50%",
        resizeMode: "contain"
    },
    card: {
        borderRadius: 15,
        borderColor: "black",
        borderWidth: 3,
        width: "100%",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#57606f",
    },
    title: {
        flex: 1,
        top: "10%",
        fontSize: 30,
    },
    input: {
        borderRadius: 3,
        borderWidth: 3,
        borderColor: "#eccc68",
        justifyContent: "center",
        height: "40%",

    },
    buttonautoload: {
        borderColor: "#2f3542",
        color: "#ffffff",
        backgroundColor: "#dfe4ea",
        padding: 5,
    },
    textbutton:{
      fontSize: 17,

    },
    inputname: {
        borderRadius: 3,
        borderWidth: 3,
        borderColor: "#eccc68",
        justifyContent: "center",
        height: "40%"
    },
    viewinput: {
        width: "100%",
        height: "10%",
    },
    viewbutton: {
        height: "5%",
        bottom: 10,
    }
});
